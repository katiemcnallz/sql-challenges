--First challenge field types, got these by running the 'chkfields'
script and finding out each columns characters and decimals.

CREATE TABLE [dbo].[Q1A](
	[ticker] [varchar](4) NOT NULL,
	[date] [varchar](12) NOT NULL,
	[open] [decimal](8, 4) NOT NULL,
	[high] [decimal](8, 4) NOT NULL,
	[low] [decimal](8, 4) NOT NULL,
	[close] [decimal](8, 4) NOT NULL,
	[vol] [varchar](7) NOT NULL
) ON [PRIMARY]


--For the second challenge when importing the csv file i also ran the
chkfield script to find out what field types were best.

CREATE TABLE [dbo].[Q2](
	[Date] [varchar](10) NOT NULL,
	[Time] [varchar](4) NOT NULL,
	[Open] [decimal](5, 2) NOT NULL,
	[High] [decimal](5, 2) NOT NULL,
	[Low] [decimal](5, 2) NOT NULL,
	[Close] [decimal](5, 2) NOT NULL,
	[Volume] [varchar](7) NOT NULL
) ON [PRIMARY]

Assumptions; I used the 'close' column as the 'trade-price' due to it being
the end price.

For CALCULATING VOLUME Weighted Prices - Challenge 1 of 2 my procedure
is in question1 file.

For IDENTIFYING THE BIGGEST DAILY TRADING RANGES – Challenge 2 of 2 
my procedure is in question2 file

To run my procedure for question1; exec calculating_vwap '201010110920'
To run my procedure for question2; exec SQLChallenge2
